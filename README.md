# samplecorpora

This is a data package that contains several small to medium length text corpora. It is meant to be useful as sample data for text processing code.

## Installation

Clone the repository from bitbucket, run R CMD build and R CMD INSTALL.

Install through bitbucket or github with devtools.
```{r}
devtools::install_github("avkoehl/samplecorpora")
devtools::install_bitbucket("digitalscholarship/samplecorpora")
```

## Contact

To contact creator email Arthur Koehl at avkoehl at ucdavis dot edu
